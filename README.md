di.js
=====

<b>di.js</b> is a dependecy injection and dependency management framework for
javascript.
The main goal of <b>di.js</b> is to provide dependency injection to your application, nothing more and nothing less.
It tends to be simple, small, fast and easy.

Use it in your project
----------------------

Include the JavaScript file and you're done:
    <script type="text/javascript" src="di.js"></script>

module(...)
--------


helpers
------


License
--------
Lincensed under Apache 2.0 license.
